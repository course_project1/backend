import { Injectable } from "@nestjs/common";
import { User } from "src/users/entities/users.entity";
import {genSalt, hash, compare} from 'bcrypt'

@Injectable()
export class PasswordService {

    async generateHashByPassword(password: User['password']) {
        const salt = await genSalt();
        return await hash(password, salt);
    }

    async compareHash(password: User['password'], hash: string) {
        return compare(password, hash)
    }

}