import { ApiProperty } from "@nestjs/swagger";
import { IsString, MaxLength, MinLength } from "class-validator";
import { User } from "src/users/entities/users.entity";


export class SignUpDto {
    @IsString()
    @MinLength(3)
    @MaxLength(20)
    @ApiProperty({
        type: String
    })
    password: User['password']

    @IsString()
    @MinLength(3)
    @MaxLength(20)
    @ApiProperty({
        type: String
    })    
    username: User['username']
}