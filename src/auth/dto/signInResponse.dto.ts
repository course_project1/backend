import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";
import { User } from "src/users/entities/users.entity";


export class SignInResponseDto {
    @ApiProperty()
    user: User;

    @IsString()
    @ApiProperty()
    jwtToken: string;
}