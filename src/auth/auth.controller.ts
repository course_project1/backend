import { Body, Controller, Post } from '@nestjs/common';
import { ApiBody, ApiResponse } from '@nestjs/swagger';
import { CreateUserDto } from '../users/dto/createUser.dto';
import { AuthService } from './auth.service';
import { SignInDto } from './dto/signIn.dto';
import { SignInResponseDto } from './dto/signInResponse.dto';
import { SignUpDto } from './dto/signUp.dto';
import { SignUpResponseDto } from './dto/signUpResponse.dto';


@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signIn')
  @ApiBody({
      type: SignInDto
  })
  @ApiResponse({
      type: SignInResponseDto
  })
  async login(@Body() dto: SignInDto) {
    return this.authService.login(dto);
  }

  @Post('signUp')
  @ApiBody({
    type: SignUpDto
    })
    @ApiResponse({
        type: SignUpResponseDto
    })
  async registration(@Body() dto: CreateUserDto){
    return this.authService.register(dto);
  }
}
