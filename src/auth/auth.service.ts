import { BadRequestException, HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { PasswordService } from "src/password/password.service";
import { CreateUserDto } from "src/users/dto/createUser.dto";
import { UsersService } from "src/users/users.service";
import { SignInDto } from "./dto/signIn.dto";

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService, 
        private jwtService: JwtService, 
        private passwordServive: PasswordService
    ) {}

    async login(dto: SignInDto) {
        const user = await this.usersService.findOne({where: {
            username: dto.username
        }})

        if (!user) {
            throw new HttpException('User not found', HttpStatus.NOT_FOUND)
        }

        const isValidPassword = await this.passwordServive.compareHash(dto.password, user.password);

        if(!isValidPassword) {
            throw new HttpException('User password is not valid', HttpStatus.UNAUTHORIZED)
        }

        const payload = { username: user.username, id: user.id };
        return {
            user: user,
            jwtToken: this.jwtService.sign(payload),
        };

    }
    

    async register (dto: CreateUserDto) {
        const isUserAlreadyExist = await this.usersService.findOne({where: {
            username: dto.username
        }})

        if(isUserAlreadyExist) {
            throw new BadRequestException(
                `User with username: ${dto.username} already exists`,
            );
        }

        const newUser = await this.usersService.createOne(dto);

        const payload = { username: newUser.username, id: newUser.id };
        return {
            user: newUser,
            jwtToken: this.jwtService.sign(payload),
        };
    
    }
}
