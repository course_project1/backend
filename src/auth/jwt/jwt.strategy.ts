import { HttpCode, HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UsersService } from "src/users/users.service";
import { JWT_SECRET } from "./jwt.secret";


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private usersService: UsersService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: JWT_SECRET
          });
      
    }

    async validate (payload: any) {
        const userId = payload.id;

        const user = await this.usersService.findOne({where: {id: userId}})

        if (!user) {
            throw new HttpException('User not found', HttpStatus.UNAUTHORIZED)
        }

        return user
    }
}
