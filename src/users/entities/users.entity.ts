import { ApiProperty } from "@nestjs/swagger";
import { Note } from "src/notes/entities/note.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('user')
export class User {
    @PrimaryGeneratedColumn('uuid')
    @ApiProperty()
    id: string;

    @Column('varchar')
    @ApiProperty()
    username: string;

    @Column('varchar')
    @ApiProperty()
    password: string

    @OneToMany(() => Note, (note) => note.userId)
    notes: Note[]
}