import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PasswordService } from 'src/password/password.service';
import { User } from './entities/users.entity';
import { UsersService } from './users.service';


@Module({
    imports: [TypeOrmModule.forFeature([User])],
    providers: [ UsersService, PasswordService],
    exports: [UsersService, PasswordService],
})
export class UsersModule {}
