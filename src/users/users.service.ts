import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PasswordService } from 'src/password/password.service';
import { FindOneOptions, Repository } from 'typeorm';
import { CreateUserDto } from './dto/createUser.dto';
import { User } from './entities/users.entity';


@Injectable()
export class UsersService {
    constructor(@InjectRepository(User) private userRepository: Repository<User>, private passwordService: PasswordService){}

    async createOne(dto: CreateUserDto): Promise<User> {
        dto.password = await this.passwordService.generateHashByPassword(dto.password);
        return await this.userRepository.save(dto)
    }

    async findOne(options: FindOneOptions<User>) {
        return await this.userRepository.findOne(options)
    }
}
