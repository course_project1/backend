import { User } from '../entities/users.entity';
import { IsString, MaxLength, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
    @IsString()
    @MinLength(3)
    @MaxLength(20)
    @ApiProperty()
    password: User['password']

    @IsString()
    @MinLength(3)
    @MaxLength(20)
    @ApiProperty()
    username: User['username']
}