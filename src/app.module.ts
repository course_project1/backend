import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { User } from './users/entities/users.entity';
import { Note } from './notes/entities/note.entity';
import { NotesModule } from './notes/notes.module';


@Module({
  imports: [
    UsersModule,
    AuthModule,
    NotesModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      url: 'postgres://ddmdvphp:QKldPyRstkpeFWFF4snrfEwW8S7JDAhg@chunee.db.elephantsql.com/ddmdvphp',
      entities: [User, Note],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
