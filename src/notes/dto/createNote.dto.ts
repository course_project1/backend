import { ApiProperty } from "@nestjs/swagger";
import { IsString, MinLength } from "class-validator";
import { Note } from "../entities/note.entity";



export class CreateNoteDto {
    @IsString()
    @MinLength(3)
    @ApiProperty()    
    @ApiProperty({
        type: String
    })
    title: Note['title']

    @IsString()
    @MinLength(3)
    @ApiProperty({
        type: String
    })
    body: Note['body']
}