import { ApiProperty } from "@nestjs/swagger";
import { IsString, MinLength } from "class-validator";
import { Note } from "../entities/note.entity";


export class UpdateNoteDto {
    @IsString()
    @MinLength(3)
    @ApiProperty({
        type: String
    })
    title: Note['title']

    @IsString()
    @MinLength(3)
    @ApiProperty({
        type: String
    })
    body: Note['body']
}