import { Controller, Get, UseGuards, Request, Body, Param, Post, Patch, Delete } from "@nestjs/common";
import { ApiBearerAuth, ApiHeader, ApiParam, ApiProperty, ApiResponse } from "@nestjs/swagger";
import { type } from "os";
import { JwtAuthGuard } from "src/auth/jwt/jwt.guard";
import { CreateNoteDto } from "./dto/createNote.dto";
import { UpdateNoteDto } from "./dto/updateNote.dto";
import { Note } from "./entities/note.entity";
import { NotesService } from "./notes.service";

@ApiBearerAuth("JWT")
@Controller('notes')
export class NotesController {

    constructor(private notesService: NotesService) {}

    @Get()
    @UseGuards(JwtAuthGuard)
    @ApiResponse({
        type: [Note]
    })
    async getAll (@Request() req): Promise<Note[]> {
        return await this.notesService.findAll(req.user)
    }   

    @Get(':id')
    @UseGuards(JwtAuthGuard)
    @ApiResponse({
        type: Note
    })
    @ApiParam({
        name: 'id',
        type: String
    })
    async getOne (@Request() req, @Param() params): Promise<Note> {
        return await this.notesService.findOne(req.user, params.id)
    }  

    @Post()
    @UseGuards(JwtAuthGuard)
    @ApiResponse({
        type: Note
    })
    async create (@Body() dto: CreateNoteDto, @Request() req): Promise<Note> {
        return await this.notesService.create(dto, req.user)
    }  

    @Patch(':id')
    @UseGuards(JwtAuthGuard)
    @ApiResponse({
        type: Note
    })
    async update (@Request() req, @Body() dto: UpdateNoteDto, @Param() params): Promise<Note> {
        return await this.notesService.update(req.user, dto, params.id)
    }  

    @Delete(':id')
    @UseGuards(JwtAuthGuard)
    @ApiResponse({
        type: Note
    })
    @ApiParam({
        name: 'id',
        type: String,
    })
    async delete (@Request() req, @Param() params): Promise<Note> {
        return await this.notesService.delete(req.user, params.id)
    }  
}