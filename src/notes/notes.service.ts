import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "src/users/entities/users.entity";
import { Repository } from "typeorm";
import { CreateNoteDto } from "./dto/createNote.dto";
import { UpdateNoteDto } from "./dto/updateNote.dto";
import { Note } from "./entities/note.entity";


@Injectable()
export class NotesService {
    constructor(@InjectRepository(Note) private notesRepository: Repository<Note>) {}

    private async validateUserNotes(user: User, noteId: Note['id']): Promise<Note>  {
        const note = await this.notesRepository.findOneOrFail({where: {id: noteId}});

        if(note.userId !== user.id) {
            throw new HttpException("This user not have permission for note!", HttpStatus.FORBIDDEN);
        }
        
        return note
    }

    async findAll(user: User): Promise<Note[]>  {
        return await this.notesRepository.find({where: {userId: user.id}});
    }

    async findOne(user: User, noteId: Note['id']): Promise<Note> {
        return await this.validateUserNotes(user, noteId)
    }

    async update(user: User, dto: UpdateNoteDto, noteId: Note['id']): Promise<Note> {
        await this.validateUserNotes(user, noteId)

        await this.notesRepository.update(noteId, dto);
        return await this.findOne(user, noteId)
    }

    async delete(user: User, noteId: Note['id']): Promise<Note>  {
        const note = await this.validateUserNotes(user, noteId)

        return await this.notesRepository.remove(note)
    }

    async create(dto: CreateNoteDto, user: User): Promise<Note>  {

        const note = await this.notesRepository.findOne({where: {
            userId: user.id,
            title: dto.title
        }})

        if(note) {
            throw new HttpException("Note with same title already exist", HttpStatus.CONFLICT)
        }

        const preparedDto = {
            userId: user.id,
            ...dto
        }

        return await this.notesRepository.save(preparedDto)
    }
}