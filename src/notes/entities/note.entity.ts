import { ApiProperty } from "@nestjs/swagger";
import { User } from "src/users/entities/users.entity";
import { Column, Entity, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";


@Entity('note')
export class Note {
    @PrimaryGeneratedColumn('uuid')
    @ApiProperty()
    id: string;

    @Column('varchar')
    @ApiProperty()
    title: string;

    @Column('varchar')
    @ApiProperty()
    body: string;

    @ManyToOne(() => User, (user) => user.notes)
    user: User['id']

    @Column({nullable: true})
    @ApiProperty({
        type: String
    })
    userId: User['id']
}